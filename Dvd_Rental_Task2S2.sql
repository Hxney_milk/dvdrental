SELECT
  f.film_id,
  f.title,
  rental_count
FROM (
  SELECT
    i.film_id,
    COUNT(*) AS rental_count
  FROM
    rental r
    JOIN inventory i ON r.inventory_id = i.inventory_id
  GROUP BY
    i.film_id
  ORDER BY
    rental_count DESC
) AS TopMovies
JOIN film f ON f.film_id = TopMovies.film_id
LIMIT 5;
