WITH ActorFilmYears AS (
  SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    MIN(f.release_year) AS first_film_year,
    MAX(f.release_year) AS last_film_year
  FROM
    actor a
    JOIN film_actor fa ON a.actor_id = fa.actor_id
    JOIN film f ON fa.film_id = f.film_id
  GROUP BY
    a.actor_id, a.first_name, a.last_name
)
SELECT
  actor_id,
  first_name,
  last_name,
  ABS(last_film_year - first_film_year) AS years_inactive
FROM
  ActorFilmYears
ORDER BY
  years_inactive DESC;