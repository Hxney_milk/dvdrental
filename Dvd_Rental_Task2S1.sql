SELECT
  film.film_id,
  film.title,
  COUNT(*) AS rental_count
FROM
  rental
  JOIN inventory ON rental.inventory_id = inventory.inventory_id
  JOIN film ON inventory.film_id = film.film_id
GROUP BY
  film.film_id, film.title
ORDER BY
  rental_count DESC
LIMIT 5;