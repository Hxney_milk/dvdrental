SELECT
  s.store_id,
  s.staff_id,
  SUM(p.amount) AS total_revenue
FROM
  payment p
  JOIN staff s ON p.staff_id = s.staff_id
  JOIN rental r ON p.rental_id = r.rental_id
WHERE
  EXTRACT(YEAR FROM r.rental_date) = 2017
GROUP BY
  s.store_id, s.staff_id
HAVING
  SUM(p.amount) = (
    SELECT
      MAX(total_revenue)
    FROM
      (SELECT
        s2.store_id,
        s2.staff_id,
        SUM(p2.amount) AS total_revenue
      FROM
        payment p2
        JOIN staff s2 ON p2.staff_id = s2.staff_id
        JOIN rental r2 ON p2.rental_id = r2.rental_id
      WHERE
        EXTRACT(YEAR FROM r2.rental_date) = 2017
      GROUP BY
        s2.store_id, s2.staff_id) AS max_revenue
    WHERE
      max_revenue.store_id = s.store_id
  );