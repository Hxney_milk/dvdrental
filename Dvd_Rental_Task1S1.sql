WITH YearlyRevenue AS (
  SELECT
    s.store_id,
    s.staff_id,
    SUM(p.amount) AS total_revenue
  FROM
    payment p
    JOIN staff s ON p.staff_id = s.staff_id
    JOIN rental r ON p.rental_id = r.rental_id
  WHERE
    EXTRACT(YEAR FROM r.rental_date) = 2017
  GROUP BY
    s.store_id, s.staff_id
),
RankRevenue AS (
  SELECT
    store_id,
    staff_id,
    total_revenue,
    ROW_NUMBER() OVER (PARTITION BY store_id ORDER BY total_revenue DESC) AS ranking
  FROM
    YearlyRevenue
)
SELECT
  r.store_id,
  r.staff_id,
  r.total_revenue
FROM
  RankRevenue r
WHERE
  r.ranking = 1;